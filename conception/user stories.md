# 0 - backlog et exigences

## User stories

- US-1 : En tant que gestionnaire, je dois pouvoir ajouter un groupe pour organiser les stagaires.
- US-2 : En tant que gestionnaire, je dois pouvoir supprimer un groupe pour organiser les stagaires.
- US-3 : En tant que gestionnaire, je dois pouvoir modifier un groupe pour organiser les stagaires.
- US-4 : En tant que gestionnaire, je dois pouvoir récuperer un groupe pour organiser les stagaires.
- US-5 : En tant que gestionnaire, je dois pouvoir récuperer tous les groupes pour organiser les stagaires.
- US-6 : En tant que gestionnaire, je dois pouvoir récuperer les stagiaires d'un groupe pour organiser les stagaires.
- US-7 : En tant que gestionnaire, je dois pouvoir modifier les stagiaires d'un groupe pour organiser les stagaires.

## Exigences

- US-1 :
    - REQ-1-1 : Lorsqu'on envoi une requète POST sur l'url `/groupes` du web service avec dans le body un groupe valide en json, le système doit ajouter le groupe à la base de données et me renvoyer un status 200 avec le groupe en json dans le body de la réponse.
    - REQ-1-2 : Lorsqu'on envoi une requète POST sur l'url `/groupes` avec dans le body un groupe non valide en json, le système doit renvoyer un status 400.
    - REQ-1-3 : En étant connecté en tant que admin, lorsque je navigue sur la page `/groupes` de l'application web, le système doit afficher la liste des groupes.
- US-2 :
    - REQ-2-1 : Lorsqu'on envoi une requète DELETE sur l'url `/groupes/{id}` du web service avec un id existant dans l'url et les identifiants d'un adminitrateur, le système doit supprimer le groupe de la base de données et me renvoyer un status 200
    - REQ-2-2 : Lorsqu'on envoi une requète DELETE sur l'url `/groupes/{id}` avec un id n'existant pas dans la base de données et les identifiants d'un adminitrateur, le système doit me renvoyer un status 404.
    - REQ-2-3 : Lorsqu'on envoi une requète DELETE sur l'url `/groupes/{id}` avec un id non valide et les identifiants d'un adminitrateur, le système doit me renvoyer un status 400.
    - REQ-2-4 : Lorsqu'on envoi une requète DELETE sur l'url `/groupes/{id}` avec un id existant ou non et les identifiants d'un non-administrateur (formateur ou stagiaire), le système doit me renvoyer un status 403.
    - REQ-2-5 : Lorsqu'on envoi une requète DELETE sur l'url `/groupes/{id}` avec un id existant ou non et sans identifiant, le système doit me renvoyer un status 401.
    - REQ-2-6 : En étant connecté en tant que admin et sur la page `/groupes` de l'application web, lorsque je clique sur le bouton "poubelle" d'un groupe le système doit supprimer ce groupe de la base de données. Le groupe ne doit plus être visible sur la page.
- US-3 :
    - REQ-3-1 : Lorsqu'on envoi une requète PUT sur l'url `/groupes/{id}` du web service avec dans le body un groupe valide en JSON et un id existant dans l'url, le système doit modifier le groupe de la base de données et me renvoyer un status 200
    - REQ-3-2 : Lorsqu'on envoi une requète PUT sur l'url `/groupes/{id}` du web service avec dans le body un groupe valide en JSON et un id inexistant dans l'url, le système doit me renvoyer un status 404
    - REQ-3-3 : Lorsqu'on envoi une requète PUT sur l'url `/groupes/{id}` du web service avec dans le body un groupe valide en JSON et un id invalide dans l'url, le système doit me renvoyer un status 400
    - REQ-3-4 : Lorsqu'on envoi une requète PUT sur l'url `/groupes/{id}` du web service avec dans le body un groupe invalide en JSON et un id existant dans l'url, le système doit me renvoyer un status 400
- US-4 :
    - REQ-4-1 : Lorsqu'on envoi une requète GET sur l'url `/groupes/{id}` avec un id existant dans la base de données, le système doit me renvoyer un status 200 avec le groupe en json dans le body de la réponse.
    - REQ-4-2 : Lorsqu'on envoi une requète GET sur l'url `/groupes/{id}` avec un id n'existant pas dans la base de données, le système doit me renvoyer un status 404.
    - REQ-4-3 : Lorsqu'on envoi une requète GET sur l'url `/groupes/{id}` avec un id non valide, le système doit me renvoyer un status 400.


## Gherkin

```
Feature: Admin can add groups

    Scenario: Add groups works in API
        Given there exists a user with role admin
        When I prepare a request on url /groupes
        And I set method to POST
        And I put in the body a groupe in json
        And I add credentials for an admin user
        Then response status should be 200
        And the body should contain the groupe in json with an id

    Scenario: Add groups fails in API because json is invalid
        Given there exists a user with role admin
        When I prepare a request on url /groupes
        And I set method to POST
        And I put in the body a invalid groupe in json
        And I add credentials for an admin user
        Then response status should be 400

    Scenario: Add groups success in web app
        Given there exists a user with role admin
        And I'm connected as admin
        And I'm on the groupes page
        When I click on the "plus"
        And I fill the form with valid values
        And I click on "submit"
        Then the groupes page is displayed
        And the newly created groupe is visible
```


```
Feature: Admin can get a groupe

    Scenario: Get a groupe works in API
        Given there exists a user with role admin
        And a group exists in the database
        When I prepare a request on url /groupes/{id}
        And I set method to GET
        And I add credentials for an admin user
        Then response status should be 200
        And the body should contain the groupe in json format
```


# 1 - Analyse des exigences

## Matrice de tracabilité des exigences

| Exigences | Test cases | Status |
| --------- | ---------- | ------ |
| REQ-1-1 | TC-1 |  |
| REQ-1-2 | | |
| REQ-1-3 | | |
| REQ-2-1 | TC-2 | |
| REQ-2-2 | TC-3 | |
| REQ-2-3 | TC-4 | |
| REQ-2-4 | TC-6 | |
| REQ-2-5 | TC-7 | |
| REQ-2-6 | TC-8 | |
| REQ-3-1 | | |
| REQ-3-2 | | |
| REQ-3-3 | | |
| REQ-3-4 | | |
| REQ-4-1 | | |
| REQ-4-2 | TC-5 | |
| REQ-4-3 | | |

# 2 - Plannification des tests

## Plan de test

1. Introduction
2. Objectives and tasks
3. Scope
4. Testing Strategy
5. Hardware Requirements
6. Environment Requirements
7. Test Schedule
8. Control Procedures
9. Features to Be Tested



10. Features Not to Be Tested
11. Resources/Roles & Responsibilities
12. Schedules
13. Dependencies
14. Risks/Assumptions
15. Tools
16. Approvals
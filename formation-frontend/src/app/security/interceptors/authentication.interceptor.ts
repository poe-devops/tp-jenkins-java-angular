import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  constructor(private as: AuthenticationService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.as.is("AUTHENTICATED")) {
      request = request.clone({
        setHeaders: {
          Authorization: "Basic " + btoa(this.as.connectedUser.value?.email + ":" + this.as.connectedUser.value?.motDePasse)
        }
      });
    }
    return next.handle(request);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, firstValueFrom, Observable, tap } from 'rxjs';
import { Personne } from 'src/app/models/personne';
import { environment } from 'src/environments/environment';


export enum AuthenticationStatus {
  ANONYMOUS = 1,
  AUTHENTICATED = 2,
  STAGIAIRE = 6,
  FORMATEUR = 10,
  ADMIN = 14
}


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  status: BehaviorSubject<AuthenticationStatus> = new BehaviorSubject<AuthenticationStatus>(AuthenticationStatus.ANONYMOUS);
  connectedUser: BehaviorSubject<Personne|undefined> = new BehaviorSubject<Personne|undefined>(undefined);

  constructor(
    private http: HttpClient,
    private router: Router
  ) {}

  async init() {
    let user = localStorage.getItem("user");
    if (user) {
      this.connectedUser.next(JSON.parse(user));
      this.status.next(AuthenticationStatus[this.connectedUser.value?.role as keyof typeof AuthenticationStatus]);
    }
  }

  login(username: string, password: string): Observable<Personne> {
    return this.http.post<Personne>(environment.backendUrl + "/authenticate", {}, {
      params: {
        "username": username,
        "password": password
      }
    }).pipe(tap(user => {
      this.status.next(AuthenticationStatus[user.role as keyof typeof AuthenticationStatus]);
      user.motDePasse = password;
      this.connectedUser.next(user);
      localStorage.setItem("user", JSON.stringify(user));
    }));
  }

  logout() {
    this.status.next(AuthenticationStatus.ANONYMOUS);
    this.connectedUser.next(undefined);
    localStorage.removeItem("user");
    this.router.navigateByUrl("/login");
  }

  is(status: keyof typeof AuthenticationStatus): boolean {
    return (AuthenticationStatus[status]&this.status.value) == AuthenticationStatus[status];
  }

}

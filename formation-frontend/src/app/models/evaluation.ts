export interface Evaluation {
  note: number;
  commentaire: string;
}

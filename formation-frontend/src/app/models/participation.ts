import { Emargement } from "./emargement";
import { Evaluation } from "./evaluation";
import { Stagiaire } from "./stagiaires";

export interface Participation {
  id: number;
  stagiaire: Stagiaire;
  moduleNom: string;
  evaluationModule: Evaluation;
  evaluationStagiaire: Evaluation;
  emargements: Emargement[];
}

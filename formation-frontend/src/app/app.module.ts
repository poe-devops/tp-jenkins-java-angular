import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StagiairesComponent } from './pages/stagiaires/stagiaires.component';
import { FormateursComponent } from './pages/formateurs/formateurs.component';
import { ModulesComponent } from './pages/modules/modules.component';
import { GroupesComponent } from './pages/groupes/groupes.component';
import { AdminsComponent } from './pages/admins/admins/admins.component';
import { AdminComponent } from './pages/admins/admin/admin.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './pages/home/home.component';
import { ConfirmComponent } from './shared/confirm/confirm.component';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatChipsModule} from '@angular/material/chips';
import {MatSelectModule} from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSidenavModule} from '@angular/material/sidenav';

import { TitleComponent } from './shared/title/title.component';
import { SecurityModule } from './security/security.module';
import { LoginComponent } from './pages/login/login.component';
import { PrintErrorsDirective } from './shared/validation/print-errors.directive';

@NgModule({
  declarations: [
    AppComponent,
    StagiairesComponent,
    FormateursComponent,
    ModulesComponent,
    GroupesComponent,
    AdminsComponent,
    AdminComponent,
    NavbarComponent,
    HomeComponent,
    ConfirmComponent,
    TitleComponent,
    LoginComponent,
    PrintErrorsDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SecurityModule,
    MatToolbarModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatChipsModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatSidenavModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Module } from '../models/module';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class ModuleService extends GenericService<Module> {

  constructor(hc: HttpClient) {
    super(hc, environment.backendUrl + "/modules");
   }
}

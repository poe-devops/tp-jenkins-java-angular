package fr.formation.categories.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.categories.models.Personne;
import fr.formation.categories.services.PersonneService;

@RestController
public class AuthenticationController {

	@Autowired
	private PersonneService ps;

	@PostMapping("/authenticate")
	public ResponseEntity<Personne> authenticate(
			@RequestParam String username, 
			@RequestParam String password) {
		Personne p = ps.authenticate(username, password);
		if (p == null)
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		else
			return ResponseEntity.ok(p);
	}


}

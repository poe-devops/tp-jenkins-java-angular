package fr.formation.categories.dto;

import java.util.Set;

import fr.formation.categories.models.Emargement;
import fr.formation.categories.models.Evaluation;
import fr.formation.categories.models.HasId;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@SuperBuilder
public class ParticipationDto implements HasId<Integer> {
	
	@EqualsAndHashCode.Include
	protected Integer id;

	private StagiaireDto stagiaire;
	
	private ModuleDto module;
	
	protected Evaluation evaluationModule;
	
	protected Evaluation evaluationStagiaire;
	
	protected Set<Emargement> emargements;
	
}

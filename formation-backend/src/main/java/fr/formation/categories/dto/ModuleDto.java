package fr.formation.categories.dto;

import java.time.LocalDate;
import java.util.TreeSet;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import fr.formation.categories.models.HasId;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@SuperBuilder
public class ModuleDto implements HasId<Integer> {

	@EqualsAndHashCode.Include
	protected Integer id;
	
	@NotBlank
	@Length(min = 3)
	protected String nom;

	protected String description;
	
	protected String code;
	
	protected TreeSet<LocalDate> jours;
	
	private FormateurDto formateur;

	private int nbParticipations;
}

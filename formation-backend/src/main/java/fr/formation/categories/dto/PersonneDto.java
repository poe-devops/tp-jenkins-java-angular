package fr.formation.categories.dto;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import fr.formation.categories.models.HasId;
import fr.formation.categories.models.Role;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter()
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@SuperBuilder
public class PersonneDto implements HasId<Integer> {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue
	protected Integer id;
	
	@NotBlank
	@Length(min = 3)
	protected String nom;

	@NotBlank
	@Length(min = 3)
	protected String prenom;
	
	@Email
	protected String email;
	
	protected Role role;
	
}

package fr.formation.categories.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.formation.categories.models.Module;

@Repository
public interface ModuleRepository extends JpaRepository<Module, Integer> {

}

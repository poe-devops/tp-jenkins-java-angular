package fr.formation.categories.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@SuperBuilder
@Entity
public class Personne {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue
	protected int id;
	
	@NotBlank
	@Length(min = 3)
	protected String nom;

	@NotBlank
	@Length(min = 3)
	protected String prenom;
	
	@Email
	protected String email;

	private String motDePasse;
	
	protected Role role;
	
}

package fr.formation.categories.models;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@SuperBuilder
@Entity
public class Module {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue
	protected int id;
	
	@NotBlank
	@Length(min = 3)
	protected String nom;

	protected String description;
	
	protected String code;
	
	protected TreeSet<LocalDate> jours;
	
	@ManyToOne
	private Formateur formateur;

	@OneToMany(mappedBy = "module")
	@Builder.Default
	private Set<Participation> participations = new HashSet<>();
}

package fr.formation.categories.mappers;

import java.util.stream.Collectors;

import org.mapstruct.AfterMapping;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import fr.formation.categories.dto.StagiaireDto;
import fr.formation.categories.models.Stagiaire;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface StagiaireMapper extends fr.formation.categories.mappers.Mapper<Stagiaire, StagiaireDto> {


	@Mappings({
//		@Mapping(target = "nbStagiaires", source = "getStagiaires().size()")
	})
	StagiaireDto modelToDto(Stagiaire source);

	@AfterMapping
	default void computeNombreAndMoyenne(Stagiaire src, @MappingTarget StagiaireDto dto) {
		dto.setParticipationsNb(src.getParticipations().size());
	}

	@AfterMapping
	default void computeGroupesNames(Stagiaire src, @MappingTarget StagiaireDto dto) {
		dto.setGroupeNames(src.getGroupes().stream().map(g -> g.getNom()).collect(Collectors.toSet()));
	}
}

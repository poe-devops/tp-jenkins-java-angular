package fr.formation.categories.mappers;

import org.mapstruct.AfterMapping;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import fr.formation.categories.dto.GroupeDto;
import fr.formation.categories.dto.ModuleDto;
import fr.formation.categories.models.Groupe;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface ModuleMapper extends fr.formation.categories.mappers.Mapper<fr.formation.categories.models.Module, ModuleDto> {


	@Mappings({
//		@Mapping(target = "nbStagiaires", source = "getStagiaires().size()")
	})
	ModuleDto modelToDto(fr.formation.categories.models.Module source);

//	@AfterMapping
//	default void computeNombreAndMoyenne(fr.formation.categories.models.Module src, @MappingTarget ModuleDto dto) {
//		dto.setParticipationsNb(src.getParticipations().size());
//	}
}

package fr.formation.categories.mappers;

import org.mapstruct.AfterMapping;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import fr.formation.categories.dto.GroupeDto;
import fr.formation.categories.dto.ModuleDto;
import fr.formation.categories.dto.ParticipationDto;
import fr.formation.categories.models.Groupe;
import fr.formation.categories.models.Participation;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface ParticipationMapper extends fr.formation.categories.mappers.Mapper<Participation, ParticipationDto> {


	@Mappings({
//		@Mapping(target = "nbStagiaires", source = "getStagiaires().size()")
	})
	ParticipationDto modelToDto(Participation source);

//	@AfterMapping
//	default void computeNombreAndMoyenne(fr.formation.categories.models.Module src, @MappingTarget ModuleDto dto) {
//		dto.setParticipationsNb(src.getParticipations().size());
//	}
}

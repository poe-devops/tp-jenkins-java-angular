package fr.formation.categories.mappers;

import org.mapstruct.Context;

public interface Mapper<MODEL, DTO> {
	
	DTO modelToDto(MODEL source);
	
	MODEL dtoToModel(DTO source);

}

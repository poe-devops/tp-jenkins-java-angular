package fr.formation.categories.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.formation.categories.dto.PersonneDto;
import fr.formation.categories.mappers.PersonneMapper;
import fr.formation.categories.models.Personne;
import fr.formation.categories.repositories.PersonneRepository;

@Service
public class PersonneService extends GenericService<PersonneMapper, PersonneRepository, Personne, PersonneDto> {
	
	@Autowired
	private PasswordEncoder pe;
	
	public Optional<Personne> findByEmail(String email) {
		return repository.findByEmail(email);
	}

	public Personne authenticate(String username, String password) {
		Optional<Personne> u = repository.findByEmail(username);
		return u.isPresent() && pe.matches(password, u.get().getMotDePasse()) ? u.get() : null;
	}
}

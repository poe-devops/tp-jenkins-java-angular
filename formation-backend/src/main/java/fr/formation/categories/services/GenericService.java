package fr.formation.categories.services;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.categories.mappers.Mapper;

public class GenericService<MAPPER extends Mapper<MODEL, DTO>, REPOSITORY extends JpaRepository<MODEL, Integer>, MODEL, DTO> {

	@Autowired
	protected REPOSITORY repository;

	@Autowired
	protected MAPPER mapper;
	
	public Collection<DTO> findAll() {
		return repository.findAll().stream()
				.map(g -> mapper.modelToDto(g))
				.collect(Collectors.toList());
	}
	
	public Optional<DTO> findById(int id) {
		return repository.findById(id).map(g -> mapper.modelToDto(g));
	}
	
	public DTO save(DTO gd) {
		MODEL g = repository.save(mapper.dtoToModel(gd));
		return mapper.modelToDto(g);
	}
	
	public DTO update(DTO gd) {
		MODEL g = repository.save(mapper.dtoToModel(gd));
		return mapper.modelToDto(g);
	}
	
	@Transactional
	public void deleteById(Integer id) {
		repository.findById(id).ifPresentOrElse(
				m -> delete(m), 
				() -> { throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity with id " + id + " does not exists"); }
		);
	}
	
	@Transactional
	public void delete(MODEL m) {
		repository.delete(m);
	}
	
}
